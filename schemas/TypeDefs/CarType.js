const { 
    GraphQLObjectType, 
    GraphQLInt, 
    GraphQLString
} = require("graphql")

const CarType = new GraphQLObjectType({
    name: "Car",
    fields: () => ({
        car_id: { type: GraphQLInt },
        make: { type: GraphQLString },
        model: { type: GraphQLString },
        cost: { type: GraphQLString }
    })
})

module.exports = CarType
