//
// File: index.js
// Auth: Martin Burolla
// Date: 9/20/2022
// Desc: Express API server that hosts our GraphQL API.
//

const PORT = 5150
const cors = require('cors')
const express = require('express')
const app = express()
const schema = require('./schemas/index.js')
const { graphqlHTTP } = require("express-graphql")

app.use(cors())

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}))

app.listen(PORT, () => {
    console.log(`GraphQL is running on port: ${PORT}.`) 
})
