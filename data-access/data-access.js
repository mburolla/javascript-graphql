//
// File: data-access.js
// Auth: Martin Burolla
// Date: 9/20/2022
// Desc: Wrapper for MySQL database.
//

const { createPromisePool } = require('./promise-pool')

const SELECT_CAR = "select * from car"
const SELECT_CAR_FOR_ID = "select * from car where car_id = ?"

exports.selectAllCars = async () => {
    let retval = [];
    const promisePool = await createPromisePool()
    retval = await promisePool.query(SELECT_CAR)
    return retval[0]
}

exports.selectCarForId = async (carId) => {
    let retval = [];
    const promisePool = await createPromisePool()
    retval = await promisePool.query(SELECT_CAR_FOR_ID, carId)
    return retval[0][0]
}
