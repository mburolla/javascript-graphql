
CREATE TABLE `car` (
  `car_id` int NOT NULL AUTO_INCREMENT,
  `make` varchar(45) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `cost` int DEFAULT NULL,
  PRIMARY KEY (`car_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


insert into car (make, model, cost) values ('ford', 'mustang', 35000);
insert into car (make, model, cost) values ('toyota', 'prius', 40000);
