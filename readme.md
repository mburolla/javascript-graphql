# JavaScript GraphQL
GraphQL starter project using Express.

![](./docs/graphiql.png)

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Run server: `nodemon index.js`
- Launch GraphiQL: `http://localhost:5150/graphql?`
- Run Query:
```
query {
  getAllUsers {
    id, 
    first_name,
    last_name
  }
}
```
- Create a new file called `promise-pool.js` in the [data-access](./data-access/) directory based from [promise-pool.template.js](./data-access/promise-pool.template.js), and replace `xxx` with the credentials for your local MySQL database.
- Create a local MySQL database called cars and run this [script](./data-access/data.sql)

# Approach
- [MOCK_DATA.json](./MOCK_DATA.json)
- [Mutations](./schemas/mutations.js)
- [Queries](./schemas/queries.js)

# Links
- [GraphQL Tutorial Beginner - Learn GraphQL in NodeJS / ExpressJS (37m)](https://youtu.be/Dr2dDWzThK8)
- [GraphQL Tutorial - Full Guide To Making Queries (9m)](https://youtu.be/omSpI1Nu_pg)
- [Part 2 - Understanding GraphQL Queries (Fields/Variables/Arguments/Alias/Operation Name) (13m)](https://youtu.be/_hTnvOFKsUs)
- [GraphQL Language Support](https://graphql.org/code/)
- [Mockaroo.com](https://mockaroo.com/)

# GraphiQL Examples
```
query users{
  getUser(id: 12){
    id
    first_name
 	  last_name
  }
}

query cars{
  getCar(id: 1){
    car_id,
    make,
    model,
    cost
  }
}

query allUsers {
  getAllUsers {
    id, 
    first_name,
    last_name
    gender
  }
}

mutation deleteUser {
	deleteUser(id:2)
}

mutation update {
  createUser(
    first_name: "Brother",
    last_name: "Wease",
    email: "me.com",
    gender: "binary",
    ip_address: "127.0.0.1"
  ) {
    id
    first_name
    last_name
  }
}

```
